<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Friend extends Model
{

    public static function check($id){
        $value = \DB::table('friends')
            ->where([
                ['user1', '=', $id],
                ['user2', '=', \Auth::user()->id],
            ])
            ->orWhere([
                ['user1', '=', \Auth::user()->id],
                ['user2', '=', $id],
            ])
            ->get();
        return isset($value[0]);
    }
    public static function checkFriendship($id){
        $value = \DB::table('friends')
            ->where([
                ['user1', '=', $id],
                ['user2', '=', \Auth::user()->id],
                ['status1', 1],
            ])
            ->orWhere([
                ['user1', '=', \Auth::user()->id],
                ['user2', '=', $id],
                ['status2', 1],
            ])
            ->get();
        return isset($value[0]);
    }
    
    public static function getFriends(){
        return \DB::table('friends')
            ->where([
                        ['user1', \Auth::user()->id],
                        ['status2', 1],
                    ])
            ->orWhere([
                        ['user2', \Auth::user()->id],
                        ['status2', 1],
                    ])
            ->get();
    }

    public static function getFriendsId(){
        $friends = \DB::table('friends')
            ->where([
                        ['user1', \Auth::user()->id],
                        ['status2', 1],
                    ])
            ->orWhere([
                        ['user2', \Auth::user()->id],
                        ['status2', 1],
                    ])
            ->get();

        $ids = [];

        foreach ($friends as $key) {
            if($key->user1 == \Auth::user()->id){
                array_push($ids, $key->user2);
            }
            else{
                array_push($ids, $key->user1);                
            }
        }
        return $ids;
    }

    public static function getAllFriendsId(){
        $friends = \DB::table('friends')
            ->where([
                        ['user1', \Auth::user()->id],
                    ])
            ->orWhere([
                        ['user2', \Auth::user()->id],
                    ])
            ->get();
        $ids = [];
        foreach ($friends as $key) {
            if($key->user1 == \Auth::user()->id){
                array_push($ids, $key->user2);
            }
            else{
                array_push($ids, $key->user1);                
            }
        }
        return $ids;
    }

    public static function friendRequests(){
        $friends = \DB::table('friends')
            ->where([
                        ['user2', \Auth::user()->id],
                        ['status2', 0],
                    ])
            ->get();

        $ids = [];
        foreach ($friends as $key) {
            if($key->user1 == \Auth::user()->id){
                array_push($ids, $key->user2);
            }
            else{
                array_push($ids, $key->user1);                
            }
        }
        return $ids;
    }
}
