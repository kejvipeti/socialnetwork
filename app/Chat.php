<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    /** @OAS\Schema(
 *     description="Chat model",
 *     title="Chat model",
 *     type="object",
 *     required={"name", "photoUrls"},
 *     @OAS\Xml(
 *         name="Chat"
 *     )
 * )
 **/
    /**
 * @SWG\Swagger(
 *     schemes={"http","https"},
 *     host="api.host.com",
 *     basePath="/",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="This is my website cool API",
 *         description="Api description...",
 *         termsOfService="",
 *         @SWG\Contact(
 *             email="contact@mysite.com"
 *         ),
 *         @SWG\License(
 *             name="Private License",
 *             url="URL to the license"
 *         )
 *     ),
 *     @SWG\ExternalDocumentation(
 *         description="Find out more about my website",
 *         url="http..."
 *     )
 * )
 */

    public function messages()
    {
        return $this->hasMany('App\Message', 'chat', 'id');
    }
    public function user_1()
    {
        return $this->hasOne('App\User', 'id', 'user1');
    }
    public function user_2()
    {
        return $this->hasOne('App\User', 'id', 'user2');
    }
}
