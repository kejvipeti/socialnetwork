<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }    
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
