<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany('App\Post');
    }
    public function image()
    {
        return $this->hasOne('App\Image', 'id', 'image_id');
    }
    public function chat1()
    {
        return $this->hasMany('App\Chat', 'user1', 'id');
    }
    public function chat2()
    {
        return $this->hasMany('App\Chat', 'user2', 'id');
    }
    public function messages()
    {
        return $this->hasMany('App\Messages', 'sent', 'id');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function friend2()
    {
        return $this->hasMany('App\Friend', 'user2', 'id');
    }
    public function friend1()
    {
        return $this->hasMany('App\Friend', 'user1', 'id');
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
