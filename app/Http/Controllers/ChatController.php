<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Message;

class ChatController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
    	$chats = \Auth::user()->chat1;
    	$chats = $chats->merge(\Auth::user()->chat2);

        return view('chat')->with(compact('chats'));
    }
    public function send(Request $request){
    	$msg = new Message;
    	$msg->message = $request->msg;
    	$msg->chat = $request->chat;
    	$msg->sent = \Auth::user()->id;
    	$msg->save();
	   	return Redirect::back();
    }
}
