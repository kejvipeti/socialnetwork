<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Friend;
use App\Post;
use App\User;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
 * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ids = Friend::getFriendsId();
        array_push($ids, \Auth::user()->id);
        $posts = Post::get()
            ->whereIn('user_id', $ids)
            ->sortByDesc('created_at');
        return view('home')->with(compact('posts'));
    }
    public function profile()
    {
        $ids = Friend::getFriendsId();
        $friends = User::get()
            ->whereIn('id', $ids);
        $ids = Friend::friendRequests();
        $requests = User::get()
            ->whereIn('id', $ids);
        $user = \Auth::user();

        return view('profile')->with(compact('friends','requests', 'user'));
    }
    public function search(Request $request)
    {
        $ids = Friend::getAllFriendsId();

        array_push($ids, \Auth::user()->id);
        if(count($ids) != 0){
            $results = User::whereNotIn('id', $ids)
                ->where('name', 'like', '%' . $request->search . '%')
            ->with('image')
                ->get();
        }
        else{            
            $results = User::where('name', 'like', '%' . $request->search . '%')
            ->where('id', '<>', \Auth::user()->id)
            ->with('image')
            ->get();
        }
        return view('search')->with(compact('results'));
    }
    public function exel()
    {
        $users = User::select('id', 'name', 'email', 'created_at')->get();

        $usersArray = [];

        $usersArray[] = ['ID', 'Name', 'Email', 'Created At'];

        foreach ($users as $user) {
            $usersArray[] = $user->toArray();
        }

        Excel::create('users', function($excel) use ($usersArray){
            
            $excel->setTitle('User');

            $excel->sheet('sheet1', function($sheet) use ($usersArray) {
                $sheet->fromArray($usersArray, null, 'A1', false, false);
            })->download('xlsx');
        });
    }
}
