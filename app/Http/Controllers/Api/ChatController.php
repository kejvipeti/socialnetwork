<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Chat;
use App\Message;

class ChatController extends Controller
{
	    /**
     * @SWG\Get(
     *     path="/chat",
     *     tags={"chat"},
     *     summary="Show chat",
     *     operationId="showChat",
     *     @SWG\Response(
     *         response=405,
     *         description="Invalid input"
     *     ),
     *     security={ },
     * )
     */
   public function show(){

    	try{

	    	$chats = \Auth::user()->chat1;
	    	$chats = $chats->merge(\Auth::user()->chat2);

	    	return response()->json($chats,200);

	    }catch(JWTException $e){
	    	return response()->json(['error' => 'Something wrong happened!'], 400);
	    }
    }
    public function store(Request $request){
        try{
            $ch = Chat::where('id', '=', $request->chat);
            if(!$ch->first()){
                return response()->json(['error' => 'Chat doesn\'t exist!'], 400);
            }
            $msg = new Message;
            $msg->message = $request->msg;
            $msg->chat = $request->chat;
            $msg->sent = \Auth::user()->id;
            $msg->save();

            return response()->json(['error' => 'Success!'],200);

        }catch(JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 400);
        }
    }
    public function showMessages($id){
        try{
            $chat = Chat::where('id', '=', $id);
            if($chat->first()){
                $chat = $chat->first();
                $messages = $chat->messages;
                return response()->json($messages,200);
            }
            else{
                return response()->json(['error' => 'Something went wrong'],400);
            }


        }catch(JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 400);
        }
    }
}
