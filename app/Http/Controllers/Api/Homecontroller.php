<?php

namespace App\Http\Controllers\Api;

use App\Friend;
use App\Http\Controllers\Controller;
use App\Post;
use App\User;
use Tymon\JWTAuth\Exceptions\JWTException;

class Homecontroller extends Controller
{
    public function homeFeed()
    {
        try{
            $ids = Friend::getFriendsId();
            array_push($ids, \Auth::user()->id);
            $all_posts = Post::whereIn('user_id', $ids)
            	->join('users', 'users.id', 'posts.user_id')
            	->select('posts.*', 'users.name')
                ->get()
                ->sortByDesc('created_at');
                $posts = [];
            foreach ($all_posts as $post) {
                if($post->image){
                    array_push($posts, array_merge($post->toArray(), $post->image->toArray()));
                }else{
                    array_push($posts, $post->toArray());
                }
                
            }
            return response()->json($posts, 200);

        }catch(JWTException $e) {
            return response()->json(['error' => 'Something wrong happened!'], 400);
        }
    }

    public function profile()
    {
        try{
            $user = \Auth::user();
            $user_image = [];
            $user_image = array_merge($user->toArray(), $user->image->toArray());
            return response()->json( $user_image, 200);
        }
        catch (JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 400);
        }
    }

    public function posts()
    {
        try{
            $user = \Auth::user();
            return response()->json($user, 200);
        }
        catch (JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 400);
        }
    }
    public function search($search)
    {
        try{
               $results = User::join('images', 'images.id', '=', 'users.image_id')
               	    ->select('users.*', 'images.path')               
                    ->where('name', 'like', '%' . $search . '%')
                    ->get();
              return response()->json($results, 200);
        }catch(JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 400);
        }
    }
}
