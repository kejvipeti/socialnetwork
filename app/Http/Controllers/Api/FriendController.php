<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Friend;
use App\Message;
use App\Chat;
use App\User;

class FriendController extends Controller
{
    public function show()
    {
        try {

            $ids = Friend::getFriendsId();
            $friends = User::join('images', 'images.id', '=', 'users.image_id')
            ->select('users.*', 'images.path')
            ->where('users.id', $ids)
            ->get();

            return response()->json($friends, 200);

        } catch (JWTException $e) {
            return response()->json(['error' => 'Something wrong happened!'], 401);
        }
    }

    public function store(Request $request)
    {
        try {
            $check = Friend::check($request['id']);
            if (!$check && $request['id'] != \Auth::user()->id && User::where('id', $request['id'])->first()) {
                $friend = new Friend;
                $friend->user1 = \Auth::user()->id;
                $friend->user2 = $request['id'];
                $friend->status1 = 1;
                $friend->save();
                $chat = new Chat;
                $chat->user1 = \Auth::user()->id;
                $chat->user2 = $request['id'];
                $chat->save();

                return response()->json([ 'error' => 'Friend added successfully!'], 200);
            } else {
                return response()->json([ 'error' => 'Something went wrong!'], 400);
            }

        } catch (JWTException $e) {
            return response()->json(['error' => 'Something wrong happened!'], 401);
        }

    }

    public function edit($id)
    {
        try {
            $check = \Auth::user()
                ->friend2
                ->where('user1', $id)
                ->where('status2', 0)
                ->first();
            if (isset($check)) {
                Friend::where([
                    ['user1', '=', $id],
                    ['user2', '=', \Auth::user()->id],
                ])
                    ->orWhere([
                        ['user1', '=', \Auth::user()->id],
                        ['user2', '=', $id],
                    ])
                    ->update(['status2' => 1]);
                return response()->json(['error' => 'Friend request accepted successfully'], 200);
            } else {
                return response()->json(['error' => 'Something went wrong'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Something went wrong!'], 400);
        }

    }

    public function destroy($id)
    {

        if (Friend::check($id)) {
            Friend::where([
                ['user1', '=', $id],
                ['user2', '=', \Auth::user()->id],
            ])
                ->orWhere([
                    ['user1', '=', \Auth::user()->id],
                    ['user2', '=', $id],
                ])
                ->delete();
            $chat = Chat::where([
                ['user1', '=', $id],
                ['user2', '=', \Auth::user()->id],
            ])
                ->orWhere([
                    ['user1', '=', \Auth::user()->id],
                    ['user2', '=', $id],
                ])->first();
            Message::where('chat', $chat->id)->delete();
            $chat->delete();
            return response()->json(['error' => 'Friendship deleted'], 200);
        } else {
            return response()->json(['error' => 'Something went wrong'], 400);
        }
    }

    public function showSingle($id){
        try{
            if(Friend::checkFriendship($id)){
                $friend = User::where('id', '=', $id)->first();
                return response()->json($friend,200);
            }else{
                return response()->json(['error' => 'You are not friends'],200);
            }

        }catch(JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 401);
        }
    }

    public function friendsRequests(){
        try{
            $ids = Friend::friendRequests();
            $requests = User::join('images', 'images.id', '=', 'users.image_id')
               	    ->select('users.*', 'images.path')
               	    ->where('users.id', $ids)
               	    ->get();
            return response()->json($requests, 200);
        }catch(JWTException $e){
            return response()->json(['error' => 'Something wrong happened!'], 401);
        }
    }
}
