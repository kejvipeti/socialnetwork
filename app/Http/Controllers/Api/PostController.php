<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\User;
use App\Post;
use App\Friend;
use App\Image;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    public function index(Request $request){

    	$post = new Post;
    	if(isset($request->content)){
            $post->content = $request->content;
            $post->user_id = \Auth::user()->id;
    		if(isset($request->image)){
    			$img = time().'.'.$request->image->getClientOriginalExtension();
    			$request->image->move(public_path('img'), $img);
    			$createImg = new Image;
    			$createImg->path = $img;
    			$createImg->save();
    			$post->image_id = $createImg->id;
    		}

    		$post->save();
    		return response()->json(['error' => 'Post created successfully'], 200);
    	}
    	else{
    		return response()->json(['error' => 'Please check your input.'], 400);
    	}
    }
    public function show()
    {
        $posts = \Auth::user()->posts;
        return response()->json($posts, 200);
    }
}
