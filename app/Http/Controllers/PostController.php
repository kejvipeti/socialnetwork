<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Image;
use Illuminate\Support\Facades\Redirect;

class PostController extends Controller
{    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){
    	$post = new Post;
    	if(isset($request->content)){
            $post->content = $request->content;
            $post->user_id = \Auth::user()->id;
    		if(isset($request->image)){
    			$img = time().'.'.$request->image->getClientOriginalExtension();
    			$request->image->move(public_path('img'), $img);
    			$createImg = new Image;
    			$createImg->path = $img;
    			$createImg->save();
    			$post->image_id = $createImg->id;
    		}
    		$post->save();
    		return Redirect::back()->withErrors('Success!');
    	}
    	else{
    		return Redirect::back()->withErrors('Error! Check your input');
    	}
    }
}
