<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Friend;
use App\User;
use App\Chat;
use App\Message;

class FriendsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create($id){
    	$check = Friend::check($id);
    	if(!$check && $id != \Auth::user()->id){
	    	$friend = new Friend;
	    	$friend->user1 = \Auth::user()->id;
            $friend->user2 = $id;
            $friend->status1 = 1; 
	    	$friend->save();
            $chat = new Chat;
            $chat->user1 = \Auth::user()->id;
            $chat->user2 = $id;
            $chat->save();
	    	return Redirect::back()->withErrors('Success!');
    	}
    	else{
	    	return Redirect::back()->withErrors('Something Went wrong!');
    	}
    	
    }
    public function delete($id){
        $check = Friend::check($id);
        if ($check) {
            Friend::where([
                        ['user1', '=', $id],
                        ['user2', '=', \Auth::user()->id],
                    ])
            ->orWhere([
                        ['user1', '=', \Auth::user()->id],
                        ['user2', '=', $id],
                    ])
            ->delete();
            $chat = Chat::where([
                        ['user1', '=', $id],
                        ['user2', '=', \Auth::user()->id],
                    ])
            ->orWhere([
                        ['user1', '=', \Auth::user()->id],
                        ['user2', '=', $id],
                    ])->first();
            Message::where('chat', $chat->id)->delete();
            $chat->delete();
            return Redirect::back()->withErrors('Success!');
        }
        else{
            return Redirect::back()->withErrors('Something Went wrong!');
        }

    }
    public function confirm($id){
        if(Friend::check($id)){
            Friend::where([
                        ['user1', '=', $id],
                        ['user2', '=', \Auth::user()->id],
                    ])
            ->orWhere([
                        ['user1', '=', \Auth::user()->id],
                        ['user2', '=', $id],
                    ])
            ->update(['status2' => 1]);
            return Redirect::back()->withErrors('Success!');
        }
        else{
            return Redirect::back()->withErrors('Something Went wrong!');
        }
        
    }
    public function index($id){
        if(Friend::check($id)){
            $user = User::where('id', $id)->first();
            return view('profile')->with(compact('user'));
        }
        else{
            return Redirect::back()->withErrors('You are not friends yet');
        }
        
    }
}
