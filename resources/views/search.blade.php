@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
            @if($errors->any())
                <h4>{{$errors->first()}}</h4>
            @endif    
        <div class="card">
                <div class="card-header">Posts</div>

                <div class="card-body">
                    @foreach($results as $res)
                        <h1>{{$res->name}}</h1>
                        <a href='{{url("/add/{$res->id}")}}'>Add Friend</a>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
