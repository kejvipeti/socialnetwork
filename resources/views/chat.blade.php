@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Chats</div>

                <div class="card-body">
                    <ul>
                        @foreach($chats as $chat)
                                @if($chat->user1 == \Auth::user()->id)
                                    <h2 onclick="show('{{$chat->user_2->id}}')">
                                        {{$chat->user_2->name}}
                                    </h2>
                                @else
                                    <h2 onclick="show('{{$chat->user_1->id}}')">
                                        {{$chat->user_1->name}}
                                    </h2>
                                @endif<hr>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Messages</div>
                    <div class="card-body">
                        @foreach($chats as $ch)
                                 @if($chat->user1 == \Auth::user()->id)
                                    <div class="chat" id="{{$ch->user_2->id}}" style="display: none; height: 450px; overflow-y: scroll;">
                                @else
                                    <div class="chat" id="{{$ch->user_1->id}}" style="height: 450px; display: none; overflow-y: scroll;">
                                @endif
                                <form action='{{url("/send")}}' method="POST">
                                @foreach($ch->messages as $message)
                                    @if($message->sender->id == \Auth::user()->id)
                                        <p class="text-right" style="color: red">{{$message->message}}</p>
                                        <p class="text-right" >{{$message->created_at->diffForHumans()}}</p>
                                    @else
                                        <p style="color: blue">{{$message->message}}</p>
                                        <p>{{$message->created_at->diffForHumans()}}</p>
                                    @endif
                                @endforeach
                                    {{csrf_field()}}
                                    <input type="hidden" name="chat" value='{{$chat->id}}'>
                                    <textarea name="msg" class="form-control" rows="2"></textarea><br>
                                    <input  class="form-control btn-primary" type="submit" name=""><br>
                                </form>
                                </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    function first(){
    $('.chat:first').fadeIn();        
    }
    function show(id){
        $('.chat').fadeOut();
        $('#'+id).fadeIn();
    }
    window.onload = first;
</script>
@endsection
