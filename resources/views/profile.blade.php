@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
            @if($errors->any())
                <h4>{{$errors->first()}}</h4>
            @endif
            <div class="card">
                <div class="card-header">Profile</div>

                <div class="card-body text-center">
                    <img style="width: auto; height: 200px;" src="{{asset('img/'.$user->image->path)}}"><br><br>
                    <h3>{{$user->name}}</h3>
                    <h3>{{$user->email}}</h3>
                    <a href="{{url('/exel')}}">Download Exel</a>
                    <!-- <form action='{{url("/post")}}' method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <textarea name="content" class="form-control" rows="5"></textarea><br>
                        <input name="image"  class="form-control" type="file" name=""><br>
                        <input  class="form-control btn-primary" type="submit" name=""><br>
                    </form> -->
                </div>
            </div>
        </div>
    </div>
</div><br>
@if(isset($friends))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
        <div class="card">
                <div class="card-header">Friends</div>
                <div class="card-body">
                    @foreach($friends as $friend)
                        <a href='{{url("/friend/{$friend->id}")}}'><p>{{$friend->name}}</p></a>
                        <a href='{{url("/deleteFriend/{$friend->id}")}}'>Delete friend</a>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div><br>
@endif
@if(isset($requests))
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
        <div class="card">
                <div class="card-header">Requests</div>
                <div class="card-body">
                    @foreach($requests as $request)
                        <p>{{$request->name}}</p>
                        <a href='{{url("/deleteFriend/{$request->id}")}}'>Delete friend</a>
                        <a href='{{url("/confirmFriend/{$request->id}")}}'>Confirm friend</a>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div><br>
@endif

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
        <div class="card">
                <div class="card-header">Posts</div>
                <div class="card-body">
                    @foreach($user->posts->sortByDesc('created_at') as $post)
                        <h1>{{$post->content}}</h1>
                        @if(isset($post->image_id))
                            <img style="width: auto; height: 200px;" src="{{asset('img/'.$post->image->path)}}">
                        @endif
                        <p>{{$post->created_at->diffForHumans()}}</p>
                        <p>From: <a href='{{url("/friend/{$user->id}")}}'>{{$post->user->name}}</a></p>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
