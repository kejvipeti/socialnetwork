@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
            @if($errors->any())
                <h4>{{$errors->first()}}</h4>
            @endif
            <div class="card">
                <div class="card-header">Post Something</div>

                <div class="card-body">
                    <form action='{{url("/post")}}' method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <textarea name="content" class="form-control" rows="5"></textarea><br>
                        <input name="image"  class="form-control" type="file" name=""><br>
                        <input  class="form-control btn-primary" type="submit" name=""><br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">    
        <div class="card">
                <div class="card-header">Posts</div>

                <div class="card-body">
                    @foreach($posts as $post)
                        <h1>{{$post->content}}</h1>
                        @if(isset($post->image_id))
                            <img style="width: auto; height: 200px;" src="{{asset('img/'.$post->image->path)}}">
                        @endif
                        <p>{{$post->created_at->diffForHumans()}}</p>
                        <p>From: <a href='{{url("/friend/{$post->user->id}")}}'>{{$post->user->name}}</a></p>
                        <hr>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
