<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::post('/post', 'PostController@index');

Route::get('/add/{id}', 'FriendsController@create');

Route::get('/profile', 'HomeController@profile');

Route::get('/deleteFriend/{id}', 'FriendsController@delete');

Route::get('/confirmFriend/{id}', 'FriendsController@confirm');

Route::get('/friend/{id}', 'FriendsController@index');

Route::get('/search', 'HomeController@search');

Route::get('/chats', 'ChatController@index');

Route::post('/send', 'ChatController@send');

Route::get('/exel', 'HomeController@exel');
