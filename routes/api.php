<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
    //return $request->user();
//});


Route::post('/login', 'Api\LoginController@login');

Route::post('/register', 'Api\LoginController@register');

Route::group(['middleware' => ['jwt.auth']], function() {

    Route::get('/logout', 'Api\LoginController@logout');

    Route::get('/profile', 'Api\Homecontroller@profile');

    Route::get('/home', 'Api\Homecontroller@homeFeed');


    /* Post Routs */

	Route::post('/post', 'Api\PostController@index');

	Route::get('/post', 'Api\PostController@show');


	/* Friendship Routs */

    Route::get('/friends', 'Api\FriendController@show');
    
    Route::get('/requests', 'Api\FriendController@friendsRequests');

	Route::post('/friends', 'Api\FriendController@store');

	Route::put('/friends/{id}', 'Api\FriendController@edit');

	Route::delete('/friends/{id}', 'Api\FriendController@destroy');

    Route::get('/friend/{id}', 'Api\FriendController@showSingle');
    

	/* Chat Routs */

	Route::get('/chat', 'Api\ChatController@show');

    Route::post('/message', 'Api\ChatController@store');

    Route::get('/message/{id}', 'Api\ChatController@showMessages');

    /*search Routs */

    Route::get('/search/{search}', 'Api\Homecontroller@search');
});
